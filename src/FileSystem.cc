#include <node.h>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <unistd.h>

namespace FileSystem {
    v8::Local<v8::String> standardToV8String(std::string str, v8::Isolate* isolate) {
        return v8::String::NewFromUtf8(isolate, str.c_str(), v8::String::kNormalString);
    }
    
    std::string v8ValueToStandardString(v8::Local<v8::Value> v8Str, v8::Isolate* isolate) {
        v8::String::Utf8Value str(isolate, v8Str);
        std::string stdString(*str);
        return stdString;
    }

    void StandardException(std::string str, v8::Isolate* isolate) {
        isolate->ThrowException(standardToV8String(str, isolate));
    }

    void TypeErrorException(std::string errorMsg, v8::Isolate* isolate) {
        v8::Local<v8::String> error = standardToV8String(errorMsg, isolate);
        isolate->ThrowException(v8::Exception::TypeError(error));
    }

    void GetInputMethod(const v8::FunctionCallbackInfo<v8::Value>& args) {
        v8::Isolate* isolate = args.GetIsolate();

        int numArgs = args.Length();

        std::string input;

        if(numArgs > 2) {
            StandardException("0, 1 or 2 arguments are required, but " + std::to_string(numArgs) + " given.", isolate);
            return;
        }

        if(numArgs != 0) {
            std::string argument = v8ValueToStandardString(args[0], isolate);
            std::cout << argument;
        }

        getline(std::cin, input);

        if(numArgs == 2) {
            const unsigned argc = 1;

            v8::Local<v8::Context> context = isolate->GetCurrentContext();

            v8::Local<v8::Function> callback = v8::Local<v8::Function>::Cast(args[1]);
            v8::Local<v8::Value> argv[argc] = { standardToV8String(input, isolate) };

            callback->Call(context, v8::Null(isolate), argc, argv).ToLocalChecked();

            return;
        }

        v8::Local<v8::Value> returnValue = standardToV8String(input, isolate);
        args.GetReturnValue().Set(returnValue);
    }

    void ReadFileMethod(const v8::FunctionCallbackInfo<v8::Value>& args) {
        v8::Isolate* isolate = args.GetIsolate();
        v8::Local<v8::Context> context = isolate->GetCurrentContext();

        int numArgs = args.Length();

        if(numArgs == 0 || numArgs > 2) {
            StandardException("1 or 2 arguments are required, but " + std::to_string(numArgs) + " given.", isolate);
            return;
        }

        std::string path = v8ValueToStandardString(args[0], isolate);
        std::ifstream infile;
        infile.open(path);

        if(infile.fail()) {
            infile.close();
            StandardException(strerror(errno), isolate);
            return;
        }

        std::string fileContents;
        std::string str;

        getline(infile, fileContents, '\0');

        if(numArgs == 1) {
            v8::Local<v8::String> returnValue = standardToV8String(fileContents, isolate);
            args.GetReturnValue().Set(returnValue);
        } else {
            const unsigned argc = 1;

            v8::Local<v8::Function> callback = v8::Local<v8::Function>::Cast(args[1]);
            v8::Local<v8::Value> argv[argc] = { standardToV8String(fileContents, isolate) };

            callback->Call(context, v8::Null(isolate), argc, argv).ToLocalChecked();
        }
    }

    void WriteFileMethod(const v8::FunctionCallbackInfo<v8::Value>& args) {
        v8::Isolate* isolate = args.GetIsolate();
        v8::Local<v8::Context> context = isolate->GetCurrentContext();

        int numArgs = args.Length();

        if(numArgs <= 0 || numArgs > 3) {
            StandardException("1 or 2 arguments are required, but " + std::to_string(numArgs) + " given.", isolate);
            return;
        }

        std::string path = v8ValueToStandardString(args[0], isolate);
        std::string fileContents = v8ValueToStandardString(args[1], isolate);

        std::ofstream outfile;
        outfile.open(path);

        if(outfile.fail()) {
            outfile.close();
            StandardException(strerror(errno), isolate);
            return;
        }        

        outfile << fileContents;

        if(numArgs == 3) {
            const unsigned argc = 0;

            v8::Local<v8::Function> callback = v8::Local<v8::Function>::Cast(args[2]);
            v8::Local<v8::Value> argv[argc] = {  };

            callback->Call(context, v8::Null(isolate), argc, argv).ToLocalChecked();
        }
    }

    void AppendFileMethod(const v8::FunctionCallbackInfo<v8::Value>& args) {
        v8::Isolate* isolate = args.GetIsolate();
        v8::Local<v8::Context> context = isolate->GetCurrentContext();

        int numArgs = args.Length();

        if(numArgs <= 1 || numArgs > 3) {
            StandardException("1 or 2 arguments are required, but " + std::to_string(numArgs) + " given.", isolate);
            return;
        }

        std::string path = v8ValueToStandardString(args[0], isolate);
        std::string fileContents = v8ValueToStandardString(args[1], isolate);

        std::ofstream outfile;
        outfile.open(path, std::ios::app);

        if(outfile.fail()) {
            outfile.close();
            StandardException(strerror(errno), isolate);
            return;
        }        

        outfile << fileContents;

        if(numArgs == 3) {
            const unsigned argc = 0;

            v8::Local<v8::Function> callback = v8::Local<v8::Function>::Cast(args[2]);
            v8::Local<v8::Value> argv[argc] = {  };

            callback->Call(context, v8::Null(isolate), argc, argv).ToLocalChecked();
        }
    }

    

    void Initialize(v8::Local<v8::Object> exports) {
        NODE_SET_METHOD(exports, "getInput", GetInputMethod);
        NODE_SET_METHOD(exports, "readFileSync", ReadFileMethod);
        NODE_SET_METHOD(exports, "writeFileSync", WriteFileMethod);
        NODE_SET_METHOD(exports, "appendFileSync", AppendFileMethod);
    }

    NODE_MODULE(NODE_GYP_MODULE_NAME, Initialize);
}
